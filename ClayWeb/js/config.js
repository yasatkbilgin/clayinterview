﻿var base = 'https://clayapi.azurewebsites.net/';

var Config = {
    AUTHENTICATION      : base + 'api/token',
    BUILDING            : base + 'api/admin/room/all',
    ADDROOM             : base + 'api/admin/room',
    DELETEROOM          : base + 'api/admin/room',
    ADDGATE             : base + 'api/admin/gate',
    DELETEGATE          : base + 'api/admin/gate/between',
    ADDAUTH             : base + 'api/admin/authorization',
    DELETEAUTH          : base + 'api/admin/authorization',
    GETUSERS            : base + 'api/admin/users',
    GETAUTHORIZATIONS   : base + 'api/admin/authorization',
    GATESOFROOM         : base + 'api/account/gates/room',
    OPEN                : base + 'api/account/gate/open',
    HISTORY             : base + 'api/account/history',
    CREATEUSER          : base + 'api/admin/createuser'
};