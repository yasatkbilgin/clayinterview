$(document).ready(function () {
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();
});

var CLAY = {};

CLAY.Utils = {
    getQuerystring: function (key, default_) {
        if (default_ == null) default_ = "";
        key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
        var qs = regex.exec(window.location.href);
        if (qs == null)
            return default_;
        else
            return decodeURIComponent(qs[1]);
    }
};

function logout() {
    this.localStorage.removeItem("token");
    window.location.href = '/';
}

///Events
EVENT = {
    loaded          : 'app::loaded',
    nodeAdded       : 'app::nodeadded',
    gateAdded       : 'app::gateadded',
    gateDeleted     : 'app::gatedeleted',
    nodeselected    : 'app::nodeselected',
    nodedeleted     : 'app::nodedeleted',
    nodesgot        : 'app::nodesgot',
    gatesgot        : 'app::gatesgot',
    getUserAuths    : 'app::getuserauths',
    getUserHistory  : 'app:getuserhistory',
    gateRemoved     : 'app:gateremoved',
    userCreated     : 'app:usercreated',
};
///

var CL = new function () {
    this.X = function (parent, spec) {
        var child = spec._;
        child.prototype = new parent();
        child = CLExtend(child, spec);
        CLExtend(child, spec);
        child.prototype.$super = function () {
            var args = Array.prototype.slice.call(arguments);
            if (typeof args[0] == 'string') {
                parent.prototype[args[0]].call(args[1], args.slice(2));
            } else {
                parent.call(args[0], args.slice(1));
            }
        };
        return child;
    };
};

var CLExtend = function (child, spec) {
    for (var prop in spec) {
        if (prop != '_') {
            if (typeof spec[prop] === 'function') {
                child.prototype[prop] = spec[prop];
            } else {
                child[prop] = spec[prop];
            }
        }
    }
    return child;
};

var CLObject = function () {
};

CL.UIObject = CL.X(CLObject, {
    _: function (element) {
        this.element = $(element);
        this.collect();
    },

    collect: function () {

    }
});

CL.Requester = CL.X(CLObject, {
    _: function (url, success, error, contentType) {
        this.url = url;
        this.success = success;
        this.error = error;
        this.contentType = contentType || "application/json";
    },

    request: function (reqType, postData) {
        var that = this;
        $.ajax({
            type: reqType,
            url: that.url,
            data: postData,
            success: that.success,
            error: that.error,
            crossDomain: true,
            beforeSend: function (xhr) {
                var header = localStorage.getItem("token");
                if (header) {
                    xhr.setRequestHeader("Authorization", "Bearer " + header);
                }
            }
        });
    },

    get: function () {
        this.request("GET");
    },

    post: function (data) {
        this.request("POST", data);
    },

    put: function (data) {
        this.request("PUT", data);
    },

    delete: function (data) {
        this.request("DELETE", data);
    }
});


var CLEventManager = {
    events: {},

    subscribe: function (name, callback) {
        if (this.events[name]) {
            this.events[name].push(callback);
        }
        else {
            this.events[name] = new Array(callback);
        }
        return window;
    },

    unsubscribe: function (name, callback) {
        if (this.events[name]) {
            var i = this.events[name].indexOf(callback);
            if (i > -1) {
                this.events[name].splice(i, 1);
            }
            else {
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    },

    fire: function (name) {
        if (!this.events[name]) {
            return false;
        }
        for (var i = 0; i < this.events[name].length; i++) {
            this.events[name][i].apply(window, Array.prototype.slice.call(arguments, 1));
        }
    }
};

String.prototype.format = function () {
    var args = Array.prototype.slice.call(arguments);
    var a = this;
    for (var i = 0; i < args.length; i++) {
        a = a.replace(new RegExp("\\{" + i + "\\}"), args[i]);
    }
    return a;
};

String.prototype.toLocalDateString = function () {
    return new Date(this).toLocaleString();
};

CLAPI = new function () {

    this.start = function () {
        this.collect();
    };

    this.collect = function () {
        $('[uitype]').each(function (i, el) {
            var component = $(el);
            eval("new " + $(el).attr('uitype') + "(component[0]);");
        });
        CLEventManager.fire(EVENT.loaded);
    };

    document.addEventListener('DOMContentLoaded', function () {
        this.start();
    }.bind(this), false);

    window.onload = function () {
        
    }.bind(this);

};

CL.CreateUser = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.CREATEUSER, this.createResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
    },

    collect: function () {
        this.username       = $('#username');
        this.password       = $('#password');
        this.confirm        = $('#confirm');
        this.btnCreateUser  = $('#btnCreateUser');
        this.errorResponse  = $('#errorResponse');
    },

    attach: function () {
        this.btnCreateUser.on("click", this.tryToCreate.bind(this));
    },

    error: function (resp) {
        var errorKey = Object.getOwnPropertyNames(resp.responseJSON.modelState)[0];
        this.errorResponse.text(resp.responseJSON.modelState[errorKey][0]);
    },

    createResponse: function (resp) {
        this.errorResponse.text('User created');
        CLEventManager.fire(EVENT.userCreated);
    },

    tryToCreate: function () {
        this.errorResponse.text('');
        var userModel = {
            UserName: this.username.val(),
            Password: this.password.val(),
            ConfirmPassword: this.confirm.val()
        };
        this.requester.post(userModel);
    }
});

CL.DeleteRoom = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.DELETEROOM, this.deleteResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
    },

    collect: function () {
        this.deleteButton   = $('#btnDeleteRoom');
        this.roomName       = $('#toDeleteName');
    },

    attach: function () {
        this.deleteButton.on("click", this.tryDelete.bind(this));
        CLEventManager.subscribe(EVENT.nodeselected, this.nodeSelected.bind(this));
    },

    error: function () {
    },

    nodeSelected: function(node) {
        this.selectedId = node.data().room.id;
        this.roomName.val(node.data().room.name);
    },

    tryDelete: function () {
        if (!this.selectedId) {
            alert('Select a room to delete by clicking the room on the building');
        } else {
            this.requester.url = Config.DELETEROOM + "/" + this.selectedId;
            this.requester.delete();
        }
    },

    deleteResponse: function (resp) {
        if (resp) {
            CLEventManager.fire(EVENT.nodeDeleted, this.selectedId);
            this.selectedId = null;
            this.roomName.val('');
        } else {
            alert('Cannot delete room, check if room count would not be zero if deleted');
        }
    }
});

CL.UserHistory = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.HISTORY, this.dataResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
        this.getHistory();
    },

    collect: function () {
        this.list = $('#historyList');
    },

    attach: function () {
        CLEventManager.subscribe(EVENT.getUserHistory, this.getHistory.bind(this));
    },

    error: function () {
    },

    getHistory: function (userId) {
        this.requester.get();
    },

    dataResponse: function (list) {
        $("#historyList li").remove();
        var that = this;
        list.forEach(function (resp) {
            if (resp.successful) {
                $('<li class="successful collection-item"><b>{0}</b> has successfully opened gate by id <b>{1}</b> at {2}</li>'
                    .format(resp.userName, resp.gateId, resp.useDate.toLocalDateString())).appendTo(that.list);
            } else {
                $('<li class="error collection-item"><b>{0}</b> tried to open gate by id <b>{1}</b> but failed at {2}</li>'
                    .format(resp.userName, resp.gateId, resp.useDate.toLocalDateString())).appendTo(that.list);
            }
        });
    }
});

CL.Authorizations = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.GETAUTHORIZATIONS, this.dataResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
        this.userId = null;
    },

    collect: function () {
        this.list = $('#authList');
    },

    attach: function () {
        CLEventManager.subscribe(EVENT.getUserAuths, this.getAuthorizations.bind(this));
    },

    error: function() {
    },

    getAuthorizations: function (userId) {
        if (userId) {
            var url = this.requester.url;
            this.requester.url += '/' + userId;
            this.requester.get();
            this.requester.url = url;
        }
    },

    dataResponse: function (list) {
        $("#authList li").remove();
        var that = this;
        list.forEach(function (resp) {
            $('<li class="collection-item"><b>{0}</b> has authentication from <b>{1}</b> to <b>{2}</b></li>'.format(resp.userName,
                resp.gate.outer.name, resp.gate.inner.name)).appendTo(that.list);
        });
    }
});

CL.UserRoom = CL.X(CL.UIObject, {
    _: function (element) {
        this.outside = 1;
        this.$super(this, element);
        this.requester = new CL.Requester(Config.GATESOFROOM, this.dataResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
        this.getRooms();
    },

    collect: function () {
        this.list = $('#list');
        this.openResponse = $('#openResponse');
    },

    attach: function () {
        
    },

    error: function () {

    },

    getRooms: function(roomId) {
        this.roomId = roomId || localStorage.getItem("userRoom") || 1;;
        var url = this.requester.url;
        this.requester.url += '/' + (this.roomId);
        this.requester.get();
        this.requester.url = url;
    },

    dataResponse: function (resp) {
        this.list.empty();
        resp.forEach(function (gate) {
            var room = gate.outerId == this.roomId ? gate.inner : gate.outer;
            var button = $('<li class="roomButton"><a href="#!" class="waves-effect waves-light btn pink lighten-1" value={0} roomId={1}>{2}</a></li>'
                .format(gate.id, room.id, room.name));
            button.on("click", this.tryToOpen.bind(this));
            button.appendTo(this.list);
        }.bind(this));
    },

    tryToOpen: function (btn) {
        this.openResponse.text('');
        CLEventManager.fire(EVENT.getUserHistory);
        var gate = $(btn.target);
        this.latestGateId = gate.attr('value');
        this.latestRoomId = gate.attr('roomid');
        if (this.latestGateId && this.latestRoomId) {
            var requester = new CL.Requester(Config.OPEN + '/' + this.latestGateId, this.tryOpenResponse.bind(this), this.error.bind(this))
            requester.get();
        }
    },

    tryOpenResponse: function (resp) {
        if (resp) {
            this.openResponse.text('Door Opened');
            this.roomId = this.latestRoomId;
            localStorage.setItem("userRoom", this.roomId);
            this.getRooms(this.roomId);
        } else {
            this.openResponse.text('You do not have permissions to open this gate');
        }
    }
});

CL.AuthenticateUsers = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester          = new CL.Requester(Config.ADDAUTH, this.addResponse.bind(this), this.error.bind(this));
        this.requesterDelete    = new CL.Requester(Config.DELETEAUTH, this.deleteResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
        this.getUsers();
    },

    collect: function () {
        this.addButton      = $('#btnAddAuthentication');
        this.deleteButton   = $('#btnRemoveAuthentication');
        this.gates          = $('#ddGates');
        this.users          = $('#ddUsers');
        this.gates.material_select();
        this.users.material_select();
        var that = this;
        this.users.on("change", function (sel) {
            CLEventManager.fire(EVENT.getUserAuths, that.users.val());
        });
    },

    attach: function () {
        this.addButton.on("click", this.tryAdd.bind(this));
        this.deleteButton.on("click", this.tryDelete.bind(this));
        CLEventManager.subscribe(EVENT.gateAdded, this.gateAdded.bind(this));
        CLEventManager.subscribe(EVENT.gateDeleted, this.gateDeleted.bind(this));
        CLEventManager.subscribe(EVENT.gateRemoved, this.gateDeleted.bind(this));
        CLEventManager.subscribe(EVENT.gatesgot, this.gatesGot.bind(this));
        CLEventManager.subscribe(EVENT.userCreated, this.userCreated.bind(this));
    },

    userCreated: function () {
        this.getUsers();
    },

    gatesGot: function (gates) {
        for (var i = 0; i < gates.length; i++) {
            $('<option value="{0}">{1}</option>'.format(gates[i].id, gates[i].inner.name + " - " + gates[i].outer.name))
                .appendTo(this.gates);
        }
        this.reCreate();
    },

    reCreate: function () {
        this.gates.material_select();
    },

    getUsers: function() {
        var requester = new CL.Requester(Config.GETUSERS, this.userResponse.bind(this), this.error.bind(this));
        requester.get();
    },

    userResponse: function (users) {
        this.users.empty();
        for (var i = 0; i < users.length; i++) {
            $('<option value="{0}">{1}</option>'.format(users[i].id, users[i].userName))
                .appendTo(this.users);
        }
        this.users.material_select();
    },

    gateAdded: function (gate) {
        $('<option value="{0}">{1}</option>'.format(gate.id, gate.inner.name + " - " + gate.outer.name))
                 .appendTo(this.gates);
        this.reCreate();
    },

    gateDeleted: function (gateId) {
        $("#ddGates option[value='" + gateId + "']").remove();
        this.reCreate();
    },

    error: function () {

    },

    deleteResponse: function () {
        CLEventManager.fire(EVENT.getUserAuths, this.users.val());
    },

    addResponse: function (gate) {
        CLEventManager.fire(EVENT.getUserAuths, this.users.val());
    },

    validate: function (gateId, userId) {
        if (gateId && userId) {
            return true;
        }
        alert('Select gate and user');
        return false;
    },

    tryDelete: function () {
        var gateId = this.gates.val();
        var userId = this.users.val();
        if (this.validate(gateId, userId)) {
            var url = this.requesterDelete.url;
            this.requesterDelete.url += '/' + userId + '/' + gateId;
            this.requesterDelete.delete();
            this.requesterDelete.url = url;
        }
    },

    tryAdd: function () {
        var gateId = this.gates.val();
        var userId = this.users.val();
        if (this.validate(gateId, userId)) {
            var auth = {
                UserId: userId,
                GateId: gateId
            }
            this.requester.post(auth);
        }
    }
});

CL.AddGate = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.ADDGATE, this.addResponse.bind(this), this.error.bind(this));
        this.requesterDelete = new CL.Requester(Config.DELETEGATE, this.deleteResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
    },

    collect: function () {
        this.addButton      = $('#btnAddGate');
        this.deleteButton   = $('#btnDeleteGate');
        this.roomFrom       = $('#ddRoomFrom');
        this.roomTo         = $('#ddRoomTo');
        this.roomFrom.material_select();
        this.roomTo.material_select();
    },

    attach: function () {
        this.addButton.on("click", this.tryAdd.bind(this));
        this.deleteButton.on("click", this.tryDelete.bind(this));
        CLEventManager.subscribe(EVENT.nodeAdded, this.nodeAdded.bind(this));
        CLEventManager.subscribe(EVENT.nodeDeleted, this.nodeDeleted.bind(this));
        CLEventManager.subscribe(EVENT.nodesgot, this.nodesGot.bind(this));
    },

    nodesGot: function(nodes) {
        for (var i = 0; i < nodes.length; i++) {
            $('<option value="{0}">{1}</option>'.format(nodes[i].id, nodes[i].name + " - " + nodes[i].id))
                .appendTo(this.roomFrom);
            $('<option value="{0}">{1}</option>'.format(nodes[i].id, nodes[i].name + " - " + nodes[i].id))
                .appendTo(this.roomTo);
        }
        this.reCreate();
    },

    reCreate: function() {
        this.roomFrom.material_select();
        this.roomTo.material_select();
    },

    nodeAdded: function (room) {
        $('<option value="{0}" name={1}>{2}</option>'.format(room.id, room.name, room.name + " - " + room.id))
                .appendTo(this.roomFrom);
        $('<option value="{0}" name={1}>{2}</option>'.format(room.id, room.name, room.name + " - " + room.id))
                .appendTo(this.roomTo);
        this.reCreate();
    },

    nodeDeleted: function (roomId) {
        $("#ddRoomFrom option[value='" + roomId + "']").remove();
        $("#ddRoomTo option[value='" + roomId + "']").remove();
        this.reCreate();
    },

    error: function () {

    },

    deleteResponse: function () {
        if (this.lastDeleted) {
            CLEventManager.fire(EVENT.gateDeleted, this.lastDeleted);
        }
    },

    addResponse: function (gate) {
        if (gate) {
            CLEventManager.fire(EVENT.gateAdded, gate);
        }
    },

    validate: function (from, to) {
        if (from && to && from !== to) {
            return true;
        }
        alert('Select two different rooms to connect');
        return false;
    },

    tryDelete: function () {
        this.lastDeleted = null;
        var from    = this.roomFrom.val();
        var to      = this.roomTo.val();
        if (this.validate(from, to)) {
            var url = this.requesterDelete.url;
            this.requesterDelete.url += '/' + from + '/' + to;
            this.requesterDelete.delete();
            this.requesterDelete.url = url;
            this.lastDeleted = { f: from, t: to };
        }
    },

    tryAdd: function () {
        var from    = this.roomFrom.val();
        var to      = this.roomTo.val();
        if (this.validate(from, to)) {
            var gate = {
                Name: 'Gate',
                OuterId: Math.min(from, to),
                InnerId: Math.max(from, to)
            }
            this.requester.post(gate);
        }
    }
});

CL.AddRoom = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.ADDROOM, this.addResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
    },

    collect: function () {
        this.addButton  = $('#btnAddRoom');
        this.roomName   = $('#roomName');
    },

    attach: function () {
        this.addButton.on("click", this.tryAdd.bind(this));
    },

    error: function () {
    },

    tryAdd: function () {
        var name = this.roomName.val();
        if (!name) {
            alert('Type a room name');
        } else {
            var room = { Name: name };
            this.requester.post(room);
        }
    },

    addResponse: function (room) {
        CLEventManager.fire(EVENT.nodeAdded, room);
    }
});

CL.LoginManager = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.AUTHENTICATION, this.loginResponse.bind(this), this.error.bind(this),
            'application/x-www-form-urlencoded; charset=UTF-8');
        this.collect();
        this.attach();
    },

    collect: function () {
        this.username       = $('#loginUsername');
        this.password       = $('#loginPassword');
        this.loginButton    = $('#btnLogin');
        this.errorResponse  = $('#errorResponse');
    },

    attach: function () {
        this.loginButton.on("click", this.tryLogin.bind(this));
    },

    error: function () {
        this.errorResponse.text('The user name or password is incorrect');
    },

    tryLogin: function () {
        this.errorResponse.text('');
        localStorage.removeItem('token');
        localStorage.removeItem('userRoom');
        if (this.username.val() && this.password.val()) {
            this.requester.post("grant_type=password&username=" + this.username.val() + "&password=" + this.password.val());
        } else {
            this.errorResponse.text('Please enter a username and password');
        }
    },

    loginResponse: function (resp) {
        if (resp.access_token) {
            localStorage.setItem("token", resp.access_token);
            window.location.href = resp.navigate || 'user.html'
        } else {
            this.errorResponse.text(resp.error_description);
        }
    }
});

CL.Building = CL.X(CL.UIObject, {
    _: function (element) {
        this.$super(this, element);
        this.requester = new CL.Requester(Config.BUILDING, this.buildingResponse.bind(this), this.error.bind(this));
        this.collect();
        this.attach();
    },

    graph: function () {
        var that = this;
        this.cy = cytoscape({
            container: document.getElementById('building'),

            boxSelectionEnabled: false,
            autounselectify: false,
            autoungrabify: true,

            style: cytoscape.stylesheet()
              .selector('node')
              .css({
                  'content': 'data(id)'
              })
            .selector('edge')
              .css({
                  'width': 4,
                  'line-color': '#ddd',
                  'curve-style': 'bezier'
              })
            .selector('.highlighted')
              .css({
                  'background-color': '#61bffc',
                  'line-color': '#61bffc',
                  'target-arrow-color': '#61bffc',
                  'transition-property': 'background-color, line-color, target-arrow-color',
                  'transition-duration': '0.5s'
              }),

            elements: {
                nodes: that.nodes,
                edges: that.edges
            }
        });
        this.cy.on('tap', 'node', {}, function (evt) {
            var node = evt.cyTarget;
            CLEventManager.fire(EVENT.nodeselected, node);
        });
    },

    collect: function () {
        
    },

    attach: function () {
        CLEventManager.subscribe(EVENT.loaded, this.getData.bind(this));
        CLEventManager.subscribe(EVENT.nodeAdded, this.nodeAdded.bind(this));
        CLEventManager.subscribe(EVENT.nodeDeleted, this.nodeDeleted.bind(this));
        CLEventManager.subscribe(EVENT.gateAdded, this.gateAdded.bind(this));
        CLEventManager.subscribe(EVENT.gateDeleted, this.gateDeleted.bind(this));
    },

    gateAdded: function(gate) {
        this.edges.push({
            data: {
                id: gate.id,
                weight: 1,
                source: "{0} - {1}".format(gate.inner.name, gate.inner.id),
                target: "{0} - {1}".format(gate.outer.name, gate.outer.id),
                roomIds: [gate.inner.id, gate.outer.id]
            }
        });
        this.graph();
    },

    gateDeleted: function (gateInfo) {
        for (var i = 0; i < this.edges.length; i++) {
            if (this.edges[i].data.roomIds.indexOf(parseInt(gateInfo.f)) > -1 &&
                this.edges[i].data.roomIds.indexOf(parseInt(gateInfo.t)) > -1) {
                this.edges.splice(i, 1);
            }
        }
        this.graph();
    },

    nodeAdded: function (room) {
        this.nodes.push({ data: { id: "{0} - {1}".format(room.name, room.id), room: room } });
        this.graph();
    },

    nodeDeleted: function (roomId) {
        for (var i = this.edges.length - 1; i >= 0; i--) {
            if (this.edges[i].data.roomIds.indexOf(roomId) > -1) {
                var gid = this.edges[i].data.id;
                CLEventManager.fire(EVENT.gateRemoved, gid);
                this.edges.splice(i, 1);
            }
        }
        for (var i = this.nodes.length - 1; i >= 0; i--) {
            if (this.nodes[i].data.room.id == roomId) {
                this.nodes.splice(i, 1);
            }
        }
        this.graph();
    },

    getData: function () {
        this.requester.get();
    },

    error: function () {

    },

    buildingResponse: function (resp) {
        CLEventManager.fire(EVENT.nodesgot, resp.rooms);
        CLEventManager.fire(EVENT.gatesgot, resp.gates);
        this.nodes = [];
        for (var i = 0; i < resp.rooms.length; i++) {
            var room = resp.rooms[i];
            this.nodes.push({ data: { id: "{0} - {1}".format(room.name, room.id), room: room } });
        }
        this.edges = [];
        for (var i = 0; i < resp.gates.length; i++) {
            var gate = resp.gates[i];
            this.edges.push({
                data: {
                    id: gate.id,
                    weight: 1,
                    source: "{0} - {1}".format(gate.inner.name, gate.inner.id),
                    target: "{0} - {1}".format(gate.outer.name, gate.outer.id),
                    roomIds: [gate.inner.id, gate.outer.id]
                }
            });
        }
        this.graph();
    }
});