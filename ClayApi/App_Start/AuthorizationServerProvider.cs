﻿using Clay.Api.Repository;
using Clay.Data.context;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using Microsoft.Owin.Security;

namespace Clay.Api
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        #region Private members
        private const   String      ALLOW_ORIGIN_HEADER = "Access-Control-Allow-Origin";
        private const   String      ADMINROLE           = "Administrator";
        private const   String      ADMINPAGE           = "admin.html";
        #endregion

        #region Private methods
        private String[] GetUserRoles(IdentityUser user)
        {
            if (user.Roles.Count == 0)
            {
                return new String[] { };
            }
            using (var context = new ClayContext())
            {
                var roleIds = user.Roles.Select(r => r.RoleId).ToList();
                return context.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToArray();
            }
        }
        #endregion

        #region Public methods
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            AuthenticationRepository repository = new AuthenticationRepository();
            IdentityUser user = await repository.FindUser(context.UserName, context.Password);

            if (user == null)
            {
                
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Sid, user.Id));
            var props = new Dictionary<string, string>();
            foreach (var role in GetUserRoles(user))
            {
                if (role == ADMINROLE)
                {
                    props.Add("navigate", ADMINPAGE);
                }
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }
            var authProps = new AuthenticationProperties(props);
            var ticket = new AuthenticationTicket(identity, authProps);
            context.Validated(ticket);
        }
        #endregion
    }
}