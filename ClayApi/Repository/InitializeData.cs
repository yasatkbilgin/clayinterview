﻿using Clay.Data.context;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clay.Api.Repository
{
    public static class InitializeData
    {
        private const   String      ADMIN       = "admin";
        private const   String      ADMINPASS   = "admin123";
        private const   String      ADMINROLE   = "Administrator";
        private const   String      OUTSIDE     = "Outside";

        private static bool RolesSet()
        {
            using (var context = new ClayContext())
            {
                return context.Roles.Count() > 0;
            }
        }

        private static bool AdminSet()
        {
            using (var context = new ClayContext())
            {
                return context.Users.Where(u => u.UserName == ADMIN).Count() > 0;
            }
        }

        private static bool OutSideSet()
        {
            using (var context = new ClayContext())
            {
                return context.Rooms.Count() > 0;
            }
        }

        public async static void Initialize()
        {
            IdentityUser admin = null;
            if (!AdminSet())
            {
                using (var context      = new ClayContext())
                using (var userStore    = new UserStore<IdentityUser>(context))
                using (var userManager  = new UserManager<IdentityUser>(userStore))
                {
                    admin = new IdentityUser() { UserName = ADMIN };
                    userManager.Create(admin, ADMINPASS);
                }
            }
            else
            {
                using (var context = new ClayContext())
                {
                    admin = context.Users.Where(u => u.UserName == ADMIN).First();
                }
            }
            if (!RolesSet())
            {
                using (var context      = new ClayContext())
                using (var userStore    = new UserStore<IdentityUser>(context))
                using (var userManager  = new UserManager<IdentityUser>(userStore))
                {
                    var roleStore = new RoleStore<IdentityRole>(context);
                    await roleStore.CreateAsync(new IdentityRole(ADMINROLE));
                    userManager.AddToRole(admin.Id, ADMINROLE);
                }
            }
            if (!OutSideSet())
            {
                using (var context = new ClayContext())
                {
                    context.Rooms.Add(new Data.entity.Room()
                    {
                        Name    = OUTSIDE
                    });
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}