﻿using Clay.Api.Models;
using Clay.Data.context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using Clay.Data.entity;

namespace Clay.Api.Repository
{
    public class RoomRepository
    {
        #region Private members
        private const long      OUTSIDEID = 1;
        #endregion

        #region Constructors
        public RoomRepository() { }
        #endregion

        #region Public methods
        public async Task<PathModel> GetBuilding()
        {
            using (var context = new ClayContext())
            {
                var allRooms = await context.Rooms.ToListAsync();
                var allGates = await context.Gates.ToListAsync();
                return new PathModel()
                {
                    Rooms = allRooms.Select(r => r.ToRoomModel()).ToArray(),
                    Gates = allGates.Select(g => g.ToGateModel()).ToArray()
                };
            }
        }

        public async Task<GateModel[]> GetRoomsGates(long roomId)
        {
            using (var context = new ClayContext())
            {
                var gates = await context.Gates.
                        Where(g => g.InnerRoomId == roomId || g.OuterRoomId == roomId).ToArrayAsync();
                return gates.Select(g => g.ToGateModel()).ToArray();
            }
        }

        public async Task<bool> Delete(long Id)
        {
            await new GateRepository().DeleteByRoomId(Id);
            await new GateAuthorizationRepository().DeleteByRoomId(Id);
            using (var context = new ClayContext())
            {
                if (context.Rooms.Count() == 1 || Id == OUTSIDEID)
                {
                    return false;
                }
                var room = new Room() { Id = Id };
                context.Rooms.Attach(room);
                context.Rooms.Remove(room);
                await context.SaveChangesAsync();
                return true;
            }
        }

        public async Task<RoomModel> Insert(RoomModel roomModel)
        {
            using (var context = new ClayContext())
            {
                var entity = new Data.entity.Room()
                {
                    Name = roomModel.Name
                };
                context.Rooms.Add(entity);
                await context.SaveChangesAsync();
                return entity.ToRoomModel();
            }
        }
        #endregion
    }
}