﻿using Clay.Api.Models;
using Clay.Data.context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace Clay.Api.Repository
{
    public class GateHistoryRepository
    {
        #region Private members
        #endregion

        #region Constructors
        public GateHistoryRepository() { }
        #endregion

        #region Public methods
        public async Task<GateHistoryModel[]> GetHistoriesByGateId(long gateId)
        {
            using (var context = new ClayContext())
            {
                return await (from h in context.GateHistories
                              join g in context.Gates on h.GateId equals g.Id
                              join u in context.Users on h.UserId equals u.Id
                              orderby h.UseDate descending
                              where h.GateId == gateId select
                                new GateHistoryModel() {
                                    UserName    = u.UserName,
                                    GateName    = g.Name,
                                    GateId      = g.Id,
                                    UseDate     = h.UseDate,
                                    Successful  = h.Successful
                                }).ToArrayAsync();
            }
        }

        public async Task<GateHistoryModel[]> GetHistoriesByUserId(String userId)
        {
            using (var context = new ClayContext())
            {
                return await (from h in context.GateHistories
                              join g in context.Gates on h.GateId equals g.Id
                              join u in context.Users on h.UserId equals u.Id
                              orderby h.UseDate descending
                              where u.Id == userId
                              select
                                  new GateHistoryModel()
                                  {
                                      UserName      = u.UserName,
                                      GateName      = g.Name,
                                      GateId        = g.Id,
                                      UseDate       = h.UseDate,
                                      Successful    = h.Successful
                                  }).ToArrayAsync();
            }
        }
        #endregion
    }
}