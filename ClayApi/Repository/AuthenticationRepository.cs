﻿using Clay.Api.Models;
using Clay.Data.context;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace Clay.Api.Repository
{
    public class AuthenticationRepository
    {
        #region Private members
        #endregion

        #region Constructors
        public AuthenticationRepository() { }
        #endregion

        #region Public methods
        public async Task<Object[]> GetUsers()
        {
            using (var context = new ClayContext())
            {
                return await context.Users.Select(u => new
                {
                    Id          = u.Id,
                    UserName    = u.UserName
                }).ToArrayAsync();
            }
        }

        public async Task<IdentityResult> Register(UserModel userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            using (var context      = new ClayContext())
            using (var userStore    = new UserStore<IdentityUser>(context))
            using (var userManager  = new UserManager<IdentityUser>(userStore))
            {
                return await userManager.CreateAsync(user, userModel.Password);
            }
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            using (var context      = new ClayContext())
            using (var userStore    = new UserStore<IdentityUser>(context))
            using (var userManager  = new UserManager<IdentityUser>(userStore))
            {
                return await userManager.FindAsync(userName, password);
            }
        }

        public IdentityUser GetUser(string Id)
        {
            using (var context      = new ClayContext())
            using (var userStore    = new UserStore<IdentityUser>(context))
            using (var userManager  = new UserManager<IdentityUser>(userStore))
            {
                return userManager.FindById(Id);
            }
        }
        #endregion
    }
}