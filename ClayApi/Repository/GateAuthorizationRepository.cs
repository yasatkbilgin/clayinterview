﻿using Clay.Api.Models;
using Clay.Data.context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using Clay.Data.entity;


namespace Clay.Api.Repository
{
    public class GateAuthorizationRepository
    {
        #region Private members
        #endregion

        #region Constructors
        public GateAuthorizationRepository() { }
        #endregion

        #region Public methods
        public async Task<GateAuthorizationModel> Insert(GateAuthorizationModel gateAuthorizationModel)
        {
            using (var context = new ClayContext())
            {
                if (context.GateAuthorizations.Where(ga => ga.UserId == gateAuthorizationModel.UserId &&
                    ga.GateId == gateAuthorizationModel.GateId).Count() > 0)
                {
                    return gateAuthorizationModel;
                }
                var entity = new Data.entity.GateAuthorization()
                {
                    UserId  = gateAuthorizationModel.UserId,
                    GateId  = gateAuthorizationModel.GateId
                };
                context.GateAuthorizations.Add(entity);
                await context.SaveChangesAsync();
                return context.GateAuthorizations.Include("Gate").Include("User").
                    Single(e => e.Id == entity.Id).ToGateAuthorizationModel();
            }
        }

        public async Task Delete(long Id)
        {
            using (var context = new ClayContext())
            {
                var gateAuthorization = new GateAuthorization() { Id = Id };
                context.GateAuthorizations.Attach(gateAuthorization);
                context.GateAuthorizations.Remove(gateAuthorization);
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteByUserIdGateId(String userId, long gateId)
        {
            using (var context = new ClayContext())
            {
                var gateAuthorization = context.GateAuthorizations.Where(ga => ga.UserId == userId &&
                    ga.GateId == gateId).FirstOrDefault();
                if (gateAuthorization != null)
                {
                    context.GateAuthorizations.Attach(gateAuthorization);
                    context.GateAuthorizations.Remove(gateAuthorization);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task DeleteByGateId(long gateId)
        {
            using (var context = new ClayContext())
            {
                var gateAuthorizations = context.GateAuthorizations.Where(ga => ga.GateId == gateId);
                if (gateAuthorizations.Count() > 0)
                {
                    context.GateAuthorizations.RemoveRange(gateAuthorizations);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task DeleteByRoomId(long roomId)
        {
            using (var context = new ClayContext())
            {
                var gateAuthorizations = (from ga in context.GateAuthorizations
                                          join g in context.Gates on
                                          ga.GateId equals g.Id
                                          where (g.InnerRoomId == roomId || g.OuterRoomId == roomId)
                                          select ga);
                if (gateAuthorizations.Count() > 0)
                {
                    context.GateAuthorizations.RemoveRange(gateAuthorizations);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<GateAuthorizationModel[]> GetUsersAuthorizations(String userId)
        {
            using (var context = new ClayContext())
            {
                var data = await (from ga in context.GateAuthorizations
                              join g in context.Gates on ga.GateId equals g.Id
                              join u in context.Users on ga.UserId equals u.Id
                              where u.Id == userId
                              select new
                              {
                                  Id        = ga.Id,
                                  UserId    = ga.UserId,
                                  GateId    = ga.GateId,
                                  UserName  = u.UserName,
                                  Gate      = g
                              }).ToArrayAsync();
                return data.Select(d => new GateAuthorizationModel()
                {
                    Id          = d.Id,
                    UserId      = d.UserId,
                    GateId      = d.GateId,
                    UserName    = d.UserName,
                    Gate        = d.Gate.ToGateModel()
                }).ToArray();
            }
        }
        #endregion
    }
}