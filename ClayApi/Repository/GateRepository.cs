﻿using Clay.Api.Models;
using Clay.Data.context;
using Clay.Data.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Clay.Api.Repository
{
    public class GateRepository
    {
        #region Private members
        #endregion

        #region Constructors
        public GateRepository() { }
        #endregion

        #region Private methods
        private async Task InsertHistory(long gateId, String userId, Boolean successful)
        {
            using (var context = new ClayContext())
            {
                var gate = context.Gates.Where(g => g.Id == gateId).FirstOrDefault();
                if (gate != null)
                {
                    var historyEntity = new GateHistory()
                    {
                        GateId      = gateId,
                        UserId      = userId,
                        UseDate     = DateTime.UtcNow,
                        Successful  = successful
                    };
                    context.GateHistories.Add(historyEntity);
                    await context.SaveChangesAsync();
                }
            }
        }
        #endregion

        #region Public methods
        public async Task<GateModel> Insert(GateModel gateModel)
        {
            using (var context = new ClayContext())
            {
                var entity = new Data.entity.Gate()
                {
                    Name        = gateModel.Name,
                    InnerRoomId = gateModel.InnerId,
                    OuterRoomId = gateModel.OuterId
                };
                context.Gates.Add(entity);
                await context.SaveChangesAsync();
                return context.Gates.Include("OuterRoom").Include("InnerRoom").Single(e => e.Id == entity.Id).ToGateModel();
            }
        }

        public async Task Delete(long Id)
        {
            using (var context = new ClayContext())
            {
                var gate = new Gate() { Id = Id };
                context.Gates.Attach(gate);
                context.Gates.Remove(gate);
                await context.SaveChangesAsync();
            }
            await new GateAuthorizationRepository().DeleteByGateId(Id);
        }

        public async Task DeleteByRooms(long outerId, long innerId)
        {
            long gateId = 0;
            using (var context = new ClayContext())
            {
                var gate = context.Gates.Where(g => g.InnerRoomId == innerId && g.OuterRoomId == outerId
                    || g.InnerRoomId == outerId && g.OuterRoomId == innerId).FirstOrDefault();
                if (gate != null)
                {
                    gateId = gate.Id;
                    context.Gates.Attach(gate);
                    context.Gates.Remove(gate);
                    await context.SaveChangesAsync();
                }
            }
            if (gateId != 0)
            {
                await new GateAuthorizationRepository().DeleteByGateId(gateId);
            }
        }

        public async Task DeleteByRoomId(long roomId)
        {
            using (var context = new ClayContext())
            {
                var gates = context.Gates.Where(g => g.OuterRoomId == roomId || g.InnerRoomId == roomId);
                if (gates.Count() > 0)
                {
                    context.Gates.RemoveRange(gates);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task<Boolean> TryOpenGate(long gateId, String userId)
        {
            Boolean result = false;
            using (var context = new ClayContext())
            {
                var authorized = context.GateAuthorizations.Where(g => g.GateId == gateId && g.UserId == userId).Count() > 0;
                if (!authorized)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            await InsertHistory(gateId, userId, result);
            return result;
        }
        #endregion
    }
}