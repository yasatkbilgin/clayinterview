﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clay.Api.Models
{
    public class GateHistoryModel
    {
        public  String      UserName        { get; set; }
        public  String      GateName        { get; set; }
        public  long        GateId          { get; set; }
        public  DateTime    UseDate         { get; set; }
        public  Boolean     Successful      { get; set; }
    }
}