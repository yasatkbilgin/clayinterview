﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Clay.Api.Models
{
    public class PathModel
    {
        [Required]
        public  IEnumerable<RoomModel>      Rooms       { get; set; }

        [Required]
        public  IEnumerable<GateModel>      Gates       { get; set; }
    }
}