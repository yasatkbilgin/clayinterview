﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Clay.Api.Models
{
    public class GateAuthorizationModel
    {
        public  long            Id          { get; set; }

        [Required]
        public  String          UserId      { get; set; }

        [Required]
        public  long            GateId      { get; set; }

        public  String          UserName    { get; set; }

        public  GateModel       Gate        { get; set; }
    }
}