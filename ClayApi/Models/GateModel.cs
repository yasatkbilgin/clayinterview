﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Clay.Api.Models
{
    public class GateModel
    {
        public  long            Id          { get; set; }

        [Required]
        public  String          Name        { get; set; }

        [Required]
        public  long            OuterId     { get; set; }

        [Required]
        public  long            InnerId     { get; set; }

        public  RoomModel       Outer       { get; set; }

        public  RoomModel       Inner       { get; set; }
    }
}