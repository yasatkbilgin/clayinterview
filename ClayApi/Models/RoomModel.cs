﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Clay.Api.Models
{
    public class RoomModel
    {
        public  long            Id          { get; set; }

        [Required]
        public  String          Name        { get; set; }
    }
}