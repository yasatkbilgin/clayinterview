﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Clay.Api.Models
{
    public class UserModel
    {
        [Required]
        public  String      UserName        { get; set; }

        [Required]
        [RegularExpression(@"^(?=.*[a-zA-Z])(?=.*\d).{6,15}$", ErrorMessage = "Your password must contain at least one character and at least one digit, also it's length must be between 6 and 15")]
        [DataType(DataType.Password)]
        public  String      Password        { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public  String      ConfirmPassword { get; set; }
    }
}