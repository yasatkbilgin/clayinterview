﻿using Clay.Api.Repository;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Http;

namespace Clay.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        #region Private members
        private const   String                  APP_JSON            = "application/json";
        private         MediaTypeHeaderValue    mMediaType          = null;
        private const   String                  CLIENT_ERROR        = "{0}\"Status\":-1, \"Message\":\"{1}\"{2}";
        private         IdentityUser            mUser               = null;
        #endregion

        #region Constructors
        public BaseApiController()
            : base()
        {
            mMediaType  = new MediaTypeHeaderValue(APP_JSON);
            mUser       = GetUser((ClaimsIdentity)RequestContext.Principal.Identity);
        }
        #endregion

        #region Private methods
        private IdentityUser GetUser(ClaimsIdentity identity)
        {
            var userId = identity.Claims.Where(c => c.Type == ClaimTypes.Sid).Select(c => c.Value).FirstOrDefault();
            return !String.IsNullOrEmpty(userId) ? new AuthenticationRepository().GetUser(userId) : null;
        }
        #endregion

        #region Public methods
        public HttpResponseException CreateResponseException(Exception e, HttpStatusCode statusCode, String clientErrorMessage)
        {
            var response                            = new HttpResponseMessage(statusCode);
            response.Content                        = new StringContent(String.Format(CLIENT_ERROR, "{", clientErrorMessage, "}"));
            response.Content.Headers.ContentType    = mMediaType;
            return new HttpResponseException(response);
        }

        public HttpResponseException CreateUnAuthorizedException()
        {
            var response                            = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.Content                        = new StringContent(String.Format(CLIENT_ERROR, "{", "UnAuthorized Access", "}"));
            response.Content.Headers.ContentType    = mMediaType;
            return new HttpResponseException(response);
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
        }
        #endregion

        #region Protected properties
        protected IdentityUser User
        {
            get
            {
                return mUser;
            }
        }
        #endregion
    }
}