﻿using Clay.Api.Models;
using Clay.Api.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Clay.Api.Controllers
{
    [RoutePrefix("api/Account")]
    [Authorize]
    public class AccountController : BaseApiController
    {
        #region Private members
        #endregion

        #region Private methods
        #endregion

        #region Endpoints
        [Route("gate/open/{gateId:long}")]
        [HttpGet]
        public async Task<IHttpActionResult> Open(long gateId)
        {
            Boolean result = await new GateRepository().TryOpenGate(gateId, this.User.Id);
            return Ok(result);
        }

        [Route("history")]
        [HttpGet]
        public async Task<IHttpActionResult> Open()
        {
            var result = await new GateHistoryRepository().GetHistoriesByUserId(User.Id);
            return Ok(result);
        }

        [Route("gates/room/{roomId:long}")]
        public async Task<IHttpActionResult> GetRoomsGates(long roomId)
        {
            return Ok(await new RoomRepository().GetRoomsGates(roomId));
        }
        #endregion

        #region Protected properties
        #endregion
    }
}
