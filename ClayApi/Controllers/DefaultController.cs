﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Clay.Api.Controllers
{
    [RoutePrefix("")]
    public class DefaultController : ApiController
    {
        [Route("")]
        [HttpGet]
        public HttpResponseMessage IAmWorking()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent("One ring to bring them all and in the darkness bind them");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
    }
}
