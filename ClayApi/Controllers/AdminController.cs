﻿using Clay.Api.Models;
using Clay.Api.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Clay.Api.Controllers
{
    [RoutePrefix("api/Admin")]
    [Authorize(Roles = "Administrator")]
    public class AdminController : BaseApiController
    {
        #region Private methods
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }
            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
                if (ModelState.IsValid)
                {
                    return BadRequest();
                }
                return BadRequest(ModelState);
            }
            return null;
        }
        #endregion

        #region Room endpoints
        [Route("room/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllRooms()
        {
            var building = await new RoomRepository().GetBuilding();
            return Ok<PathModel>(building);
        }

        [Route("room")]
        [HttpPost]
        public async Task<IHttpActionResult> InsertRoom([FromBody] RoomModel roomModel)
        {
            if (roomModel == null)
            {
                throw CreateResponseException(new InvalidOperationException(), System.Net.HttpStatusCode.BadRequest, "Room information required");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(await new RoomRepository().Insert(roomModel));
        }

        [Route("room/{roomId:long}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteRoom(long roomId)
        {
            var deleted = await new RoomRepository().Delete(roomId);
            return Ok(deleted);
        }
        #endregion

        #region History endpoints
        [Route("history/gate/{gateId:long}")]
        public async Task<IHttpActionResult> GateHistoryByGateId(long gateId)
        {
            var histories = await new GateHistoryRepository().GetHistoriesByGateId(gateId);
            return Ok<GateHistoryModel[]>(histories);
        }

        [Route("history/user/{userId}")]
        public async Task<IHttpActionResult> GateHistoryByUserId(String userId)
        {
            var histories = await new GateHistoryRepository().GetHistoriesByUserId(userId);
            return Ok<GateHistoryModel[]>(histories);
        }
        #endregion

        #region Gate endpoints
        [Route("gate")]
        [HttpPost]
        public async Task<IHttpActionResult> InsertGate([FromBody] GateModel gateModel)
        {
            if (gateModel == null)
            {
                throw CreateResponseException(new InvalidOperationException(), System.Net.HttpStatusCode.BadRequest, "Gate information required");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(await new GateRepository().Insert(gateModel));
        }

        [Route("gate/{gateId:long}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteGate(long gateId)
        {
            await new GateRepository().Delete(gateId);
            return Ok();
        }

        [Route("gate/between/{outerId:long}/{innerId:long}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteGate(long outerId, long innerId)
        {
            await new GateRepository().DeleteByRooms(outerId, innerId);
            return Ok();
        }
        #endregion

        #region User endpoints
        [Route("CreateUser")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateUser(UserModel userModel)
        {
            if (userModel == null)
            {
                throw CreateResponseException(new InvalidOperationException(), System.Net.HttpStatusCode.BadRequest, "User information required");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await new AuthenticationRepository().Register(userModel);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }

        [Route("users")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUsers()
        {
            return Ok(await new AuthenticationRepository().GetUsers());
        }
        #endregion

        #region Authorization endpoints
        [Route("authorization")]
        [HttpPost]
        public async Task<IHttpActionResult> AuthorizeUser([FromBody] GateAuthorizationModel gateAuthorizationModel)
        {
            if (gateAuthorizationModel == null)
            {
                throw CreateResponseException(new InvalidOperationException(), System.Net.HttpStatusCode.BadRequest, "Gate Authorization information required");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(await new GateAuthorizationRepository().Insert(gateAuthorizationModel));
        }

        [Route("authorization/{userId}")]
        public async Task<IHttpActionResult> GetUsersAuthorizations(String userId)
        {
            var authorizations = await new GateAuthorizationRepository().GetUsersAuthorizations(userId);
            return Ok<GateAuthorizationModel[]>(authorizations);
        }

        [Route("authorization/{authorizationId:long}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteAuthorization(long authorizationId)
        {
            await new GateAuthorizationRepository().Delete(authorizationId);
            return Ok();
        }

        [Route("authorization/{userId}/{gateId:long}")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteAuthorization(String userId, long gateId)
        {
            await new GateAuthorizationRepository().DeleteByUserIdGateId(userId, gateId);
            return Ok();
        }
        #endregion
    }
}
