﻿using Clay.Api.Models;
using Clay.Data.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clay.Data.context
{
    public static class GateExtensions
    {
        public static GateModel ToGateModel(this Gate gate)
        {
            return new GateModel()
            {
                Id      = gate.Id,
                Name    = gate.Name,
                Inner   = gate.InnerRoom.ToRoomModel(),
                Outer   = gate.OuterRoom.ToRoomModel(),
                InnerId = gate.InnerRoomId,
                OuterId = gate.OuterRoomId
            };
        }
    }
}