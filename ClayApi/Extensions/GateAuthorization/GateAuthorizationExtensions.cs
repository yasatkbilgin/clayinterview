﻿using Clay.Api.Models;
using Clay.Data.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clay.Data.context
{
    public static class GateAuthorizationExtensions
    {
        public static GateAuthorizationModel ToGateAuthorizationModel(this GateAuthorization gateAuthorization)
        {
            return new GateAuthorizationModel()
            {
                Id          = gateAuthorization.Id,
                UserId      = gateAuthorization.UserId,
                UserName    = gateAuthorization.User.UserName,
                GateId      = gateAuthorization.GateId,
                Gate        = gateAuthorization.Gate.ToGateModel()
            };
        }
    }
}