﻿using Clay.Api.Models;
using Clay.Data.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clay.Data.context
{
    public static class RoomExtensions
    {
        public static RoomModel ToRoomModel(this Room room)
        {
            return new RoomModel()
            {
                Id      = room.Id,
                Name    = room.Name
            };
        }
    }
}