﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clay.Data.entity
{
    public partial class Gate : ClayEntity
    {
        [Required]
        public          String  Name                { get; set; }
        [Required]
        public          long    OuterRoomId         { get; set; }
        [Required]
        public          long    InnerRoomId         { get; set; }

        [ForeignKey("OuterRoomId")]
        public  virtual Room    OuterRoom           { get; set; }
        [ForeignKey("InnerRoomId")]
        public  virtual Room    InnerRoom           { get; set; }
    }
}
