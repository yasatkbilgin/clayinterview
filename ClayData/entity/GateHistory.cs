﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clay.Data.entity
{
    public class GateHistory : ClayEntity
    {
        [Required]
        public  String      UserId      { get; set; }

        [Required]
        public  long        GateId      { get; set; }

        [Required]
        public  DateTime    UseDate     { get; set; }

        [Required]
        public  Boolean     Successful  { get; set; }
    }
}
