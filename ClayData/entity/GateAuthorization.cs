﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clay.Data.entity
{
    public class GateAuthorization : ClayEntity
    {
        public          long            GateId      { get; set; }
        public          String          UserId      { get; set; }

        [ForeignKey("GateId")]
        public  virtual Gate            Gate        { get; set; }
        [ForeignKey("UserId")]
        public  virtual IdentityUser    User        { get; set; }
    }
}
