﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clay.Data.entity
{
    public class ClayEntity : IClayEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key(), Required]
        public  long    Id  { get; set; }
    }
}
