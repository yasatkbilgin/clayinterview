﻿using Clay.Data.entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clay.Data.context
{
    public class ClayContext : IdentityDbContext<IdentityUser>
    {
        public ClayContext() : base("ClayContext") { }

        public  DbSet<Room>                 Rooms               { get; set; }
        public  DbSet<Gate>                 Gates               { get; set; }
        public  DbSet<GateHistory>          GateHistories       { get; set; }
        public  DbSet<GateAuthorization>    GateAuthorizations  { get; set; }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Gate>()
                   .HasRequired(m => m.InnerRoom)
                   .WithMany(t => t.InnerGates)
                   .HasForeignKey(m => m.InnerRoomId)
                   .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gate>()
                        .HasRequired(m => m.OuterRoom)
                        .WithMany(t => t.OuterGates)
                        .HasForeignKey(m => m.OuterRoomId)
                        .WillCascadeOnDelete(false);
        }
    }
}
